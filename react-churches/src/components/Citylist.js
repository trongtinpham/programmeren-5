import React,{Component} from 'react';
import data from '../data/map.json';

export default class List extends Component{
    
    
    list = data.churches.filter(church => {
        return church.city === `${this.props.data.city}`
    });
    
    
    row = this.list.map(item => {
        let url = `/images/thumbnail/${item.image}`;
        let alt = `Picture of ${item.name}`;
        return(
            <tr>
            <td><img src = {url} alt ={alt}/></td>
            <td>{item.name}</td>
            <td><button onClick={() => this.props.action('Detail',item)}>Detail</button></td>
            </tr>
            )
    })
    render(){
        return(
            <main>
               <header> 
                   <h1>List of Churches in {this.data.props.city}</h1>
              </header>
            <table>{this.row}</table>
            <button onClick={() => this.props.action('Home')}>Back to Home</button>
            </main>
            
            );
    }
}