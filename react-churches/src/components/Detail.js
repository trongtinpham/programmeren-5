import React, {Component} from 'react';
import Map from './Map';
import Comment from './Comment';

class Detail extends Component {
 
    render() {
   
        let url = `/images/big/${this.props.data.image}`;
        let alt = `Foto van ${this.props.data.name}`;
        let hyp = `http://www.${this.props.data.website}/`;
        return (
            <main>
                   <header> 
                   <h1>Information on {this.props.data.name}</h1>
                   </header>
                    <div className="bigImg"><img id="idimg" src={url} alt={alt}/></div>
                    <div id="mapid">{<Map data={this.props.data} />}</div>
              
                <ul>
                    <li>Comment: {this.props.data.comments}</li>
                    <li>Type: {this.props.data.type}</li>
                    <li>Website: <a href={hyp}>{this.props.data.website}</a></li>
                    <li>Longitude: {this.props.data.long}</li>
                    <li>Latitude: {this.props.data.lat}</li>
                    <li>City: {this.props.data.city}</li>
                </ul>
                <Comment />
            
             <button onClick={() => this.props.action('List')}>List</button>
              
                  
             </main>
        );
    }
}
export default Detail; 