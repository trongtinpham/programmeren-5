import React, {Component} from 'react';

const TableBody = props => {
    const rows = props.commentsData.map((row, index) => {
        return (
            <tr key={index}>
                <td><strong>{row.username}: </strong></td>
                <td>{row.text}</td>
           </tr>
        );
    });

    return <tbody>{rows}</tbody>;
}

class Table extends Component {
    render() {
        const {commentsData, removeComment} = this.props;
        
        return (
            <table>
                <TableBody
                    commentsData={commentsData}
                    removeComment = {removeComment}
                />
            </table>
        );
    }
}

export default Table;