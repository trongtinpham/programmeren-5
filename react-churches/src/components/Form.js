import React, { Component } from 'react';

class Form extends Component {
    
    handleChange = event => {
    const { name, value } = event.target;

    this.setState({
        [name]: value
    });
}

    submitForm = () => {
    this.props.handleSubmit(this.state);
    this.setState(this.initialState);
}
    
    constructor(props) {
        super(props);

        this.initialState = {
            username: '',
            text: ''
        };

        this.state = this.initialState;
    }
    
    render() {
    const { username, text } = this.state;

    return (
        <form>
            <label>Username</label><br/>
            <input
                type="text"
                name="username"
                value={username}
                onChange={this.handleChange} /><br />
            <label>Comment</label><br />
            <input
                type="text"
                name="text"
                value={text}
                onChange={this.handleChange} /><br />
            <input
                type="button"
                value="Post comment"
                onClick={this.submitForm} />
        </form>
    );
}
}

export default Form;