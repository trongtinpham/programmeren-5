import React,{Component} from 'react';
import data from '../data/map.json';

const TableHeadings = () => {
    return (
        <tr>
            <th scope="col"></th>
            <th scope="col">Name</th>
            <th scope="col">City</th>
            <th scope="col"></th>
        </tr>
    );
}

const TableHeader = () => {
    return (
        <thead>
            <TableHeadings />
        </thead>
    );
}

const TableFooter = () => {
    return (
        <tfoot>
            <TableHeadings />
        </tfoot>
    );
}

export default class List extends Component{
    list = data.churches;
    
    row = this.list.map(item => {
        let url = `/images/thumbnail/${item.image}`;
        let alt = `Picture of ${item.name}`;
        return(
            <tr>
            <td className="thumbnail"><img src = {url} alt ={alt}/></td>
            <td>{item.name}</td>
            <td>{item.city}</td>
            <td><button onClick={() => this.props.action('Detail',item)}>Detail</button></td>
            </tr>
            )
    })
    
    render(){
        return(
            <main>
               <header> 
                   <h1>List of Churches in Belgium</h1>
              </header>
            <table className="Fulllist">
                <TableHeader />
                {this.row}
                <TableFooter />
            </table>
            <button onClick={() => this.props.action('Home')}>Back to Home</button>
            </main>
            
            );
    }
}