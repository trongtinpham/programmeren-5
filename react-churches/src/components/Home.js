import React, { Component } from 'react'

class Home extends Component {

    render() {
        return (
        <div className="App">
            <header className="App-header">
              <img src="/images/App-logo.jpg" className="App-logo" alt="Home logo" />
              <p>
                Belgian Churches
              </p>
              <button onClick={() => this.props.action('List')}>Enter</button>
            </header>
         </div>
        )
    }
}

export default Home;