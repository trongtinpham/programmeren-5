import React, {Component} from 'react';
import Table from './CommentTable';
import Form from './Form';


export default class Comment extends Component {
    state = { comments: [] }
    
    handleSubmit = comment => {
        this.setState({comments: [...this.state.comments, comment]})
    }

    render() {
        return (
           <div className="container">
                <h2>Comments</h2>
                <Table
                    commentsData={this.state.comments}
                />
           <Form handleSubmit={this.handleSubmit} />
          </div>
        )
    };
}