// Set the map variable
const myMap = L.map('map');

// Load the basemap
const myBasemap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

// Add basemap to map id
myBasemap.addTo(myMap);

// Set view of the map
myMap.setView([51.0259, 4.4775], 10);


// Make an XMLHttpRequest to the JSON data
const request = new XMLHttpRequest();
request.open('GET', 'map.json', true);

request.onload = function() {
    // Begin accessing JSON data here
    const data = JSON.parse(this.response);

    const churches = data.churches.map(church => {

        L.marker([church.lat, church.long]).bindPopup(`
        <h2>${church.name}</h2>
        <p><b>Address:</b> ${church.address}</p>
        <p><b>City:</b> ${church.city}</p>
        <p><b>Website:</b> ${church.website}</p>
        <p><b>Comments:</b> ${church.comments}</p>
    `).openPopup().addTo(myMap);
    });

    const cityCount = data.churches.reduce((sums, church) => {
        sums[church.city] = (sums[church.city] || 0) + 1;
        return sums;
    }, {});

    const sidebar = document.getElementById("cities");
    for (let city in cityCount) {
        const p = document.createElement('p');
        p.innerHTML = `<b>${city}</b> :${cityCount[city]}`;
        sidebar.appendChild(p);
    }
}

request.send();
